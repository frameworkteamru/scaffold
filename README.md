# README #

Generates CRUD code for Laravel based on the doctrine yml and their relations

### How do I get set up? ###

composer.json

    "repositories": [{
        "type": "vcs",
        "url": "https://bitbucket.org/frameworkteamru/scaffold.git"
	}]

---

    composer require dom1no/scaffold:dev-master

### How create doctrine yml metadata ###

first - set up doctrine package

    composer require doctrine/orm
    composer require symfony/yaml

create file

cli-config.php

    <?php

        require_once 'vendor/autoload.php';

        require_once 'vendor/laravel/framework/src/Illuminate/Foundation/helpers.php';

        use Doctrine\ORM\Tools\Console\ConsoleRunner;

        use Doctrine\ORM\Tools\Setup;
        use Doctrine\ORM\EntityManager;

        $isDevMode = true;

        (new Dotenv\Dotenv(__DIR__, '.env'))->load();

        $dbParams = array(
            'driver'   => 'pdo_mysql',
            'user'     => env('DB_USERNAME', 'root'),
            'password' => env('DB_PASSWORD', ''),
            'dbname'   => env('DB_DATABASE', 'test')
        );

        $classLoader = new \Doctrine\Common\ClassLoader(null, __DIR__ . '/database/doctrine/entity');
        $classLoader->register();

        $paths = array(__DIR__ . '/database/doctrine/yaml');
        $config = Setup::createYAMLMetadataConfiguration($paths, $isDevMode);

        $entityManager = EntityManager::create($dbParams, $config);

        return ConsoleRunner::createHelperSet($entityManager);

---

doctrine - create file with filename 'doctrine'

    #!/usr/bin/env php
    <?php

        include('vendor/doctrine/orm/bin/doctrine.php');


in .env file set database settings

use command

    php doctrine

    Doctrine Command Line Interface 2.5.10

    Usage:
      command [options] [arguments]

    Options:
      -h, --help            Display this help message
      -q, --quiet           Do not output any message
      -V, --version         Display this application version
          --ansi            Force ANSI output
          --no-ansi         Disable ANSI output
      -n, --no-interaction  Do not ask any interactive question
      -v|vv|vvv, --verbose  Increase the verbosity of messages: 1 for normal output, 2 for more verbose output and 3 for debug

    Available commands:
      help                            Displays help for a command
      list                            Lists commands
     dbal
      dbal:import                     Import SQL file(s) directly to Database.
      dbal:run-sql                    Executes arbitrary SQL directly from the command line.
     orm
      orm:clear-cache:metadata        Clear all metadata cache of the various cache drivers.
      orm:clear-cache:query           Clear all query cache of the various cache drivers.
      orm:clear-cache:result          Clear all result cache of the various cache drivers.
      orm:convert-d1-schema           [orm:convert:d1-schema] Converts Doctrine 1.X schema into a Doctrine 2.X schema.
      orm:convert-mapping             [orm:convert:mapping] Convert mapping information between supported formats.
      orm:ensure-production-settings  Verify that Doctrine is properly configured for a production environment.
      orm:generate-entities           [orm:generate:entities] Generate entity classes and method stubs from your mapping information.
      orm:generate-proxies            [orm:generate:proxies] Generates proxy classes for entity classes.
      orm:generate-repositories       [orm:generate:repositories] Generate repository classes from your mapping information.
      orm:info                        Show basic information about all mapped entities
      orm:mapping:describe            Display information about mapped objects
      orm:run-dql                     Executes arbitrary DQL directly from the command line.
      orm:schema-tool:create          Processes the schema and either create it directly on EntityManager Storage Connection or generate the SQL output.
      orm:schema-tool:drop            Drop the complete database schema of EntityManager Storage Connection or generate the corresponding SQL output.
      orm:schema-tool:update          Executes (or dumps) the SQL needed to update the database schema to match the current mapping metadata.
      orm:validate-schema             Validate the mapping files. 

---

generation of metadata based on an existing database

    php doctrine orm:convert-mapping -force -from-database yml ./database/doctrine/yaml

generation of models based on the level of metadata

    php doctrine orm:generate-entities ./database/doctrine/entity

mechanism for verifying the database with metadata stored in the yml

    php doctrine orm:validate-schema

database synchronization mechanism with metadata

    php doctrine orm:schema-tool:update --dump-sql --force
    
---

yml documentation

http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/reference/yaml-mapping.html

---

example:

.\database\doctrine\yaml\Product.dcm.yml

    Product:
        type: entity
        table: product

        id:
            id:
                type: integer
                id: true
                generator:
                    strategy: AUTO

        fields:
            title:       { type: string(255), nullable: true }
            content:     { type: text, nullable: false  }

        oneToMany:
            image:
                targetEntity: ProductImage
                mappedBy: product
                cascade: [persist]

.\database\doctrine\yaml\ProductImage.dcm.yml

    ProductImage:
        type: entity
        table: product__image

        id:
            id:
                type: integer
                id: true
                generator:
                    strategy: AUTO

        fields:
            filename:       { type: string(255), nullable: true }

        manyToOne:
            product:
                targetEntity: Product
                inversedBy: image
                joinColumn:
                    name: product_id
                    referencedColumnName: id

---

    php doctrine orm:generate-entities ./database/doctrine/entity
    php doctrine orm:validate-schema
    php doctrine orm:schema-tool:update --dump-sql --force

---

\config\app.php

        Dom1no\Scaffold\GeneratorsServiceProvider::class,


    php scaffold:make Product