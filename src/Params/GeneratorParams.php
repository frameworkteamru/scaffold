<?php
namespace Dom1no\Scaffold\Params;

use Illuminate\Support\Str;
use DB;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Filesystem\Filesystem;

use Dom1no\Scaffold\Namespaces\NamespaceGenerator;
use Dom1no\Scaffold\Helpers\DatabaseHelper;
use Dom1no\Scaffold\Helpers\PathHelper;

class GeneratorParams
{
    private static $entity;
    private static $prefix = null;

    private static $authenticable = [
        'User',
        'Admin'
    ];

    public static function all($entity, $prefix)
    {
        self::$entity = $entity;
        self::$prefix = $prefix;

        $params['namespaces'] = NamespaceGenerator::generateNamespaces($entity, $prefix);
        $params['table'] = DatabaseHelper::getTableName($entity);
        $params['var'] = lcfirst($entity);
        $params['entity'] = lcfirst(PathHelper::getChildOrRootEntity($entity));
        $params['class'] = $entity;
        $params['controllerClass'] = $entity . "Controller";
        $params['authenticable'] = in_array($params['class'], self::$authenticable);
        $params['viewPath'] = PathHelper::getViewPath($entity, $prefix);
        $params['parent'] = DatabaseHelper::getParentRelationship($entity);
        $params['children'] = DatabaseHelper::getChildRelationships($entity);
        $params['routePrefix'] = strtolower(PathHelper::getChildOrRootEntity(lcfirst($entity)));
        $params['viewPrefix'] = PathHelper::getViewPrefix($entity, $prefix);
        $params['doggy'] = '@';
        $params = array_merge($params, DatabaseHelper::getFillableAndOther($params['table']), DatabaseHelper::getRelationship($entity));

        return $params;
    }

    public static function allEntities()
    {
        $entityFileSystem = new Filesystem;
        $entitiesPath = base_path(config('scaffold.metadata_path'));
        $files = $entityFileSystem->files($entitiesPath);

        foreach($files as $file)
        {
            $entities[] = preg_replace('/\.[\.a-z+]+/', '', $entityFileSystem->basename($file));
        }

        if (!isset($entities))
        {
            throw new \Exception('No entities are present');
        }

        return $entities;
    }

    public static function cleanAliases()
    {
        $entities = self::allEntities();

        $aliases = array_filter(config('app.aliases'), function($alias) use ($entities){

            return !in_array($alias, $entities);

        }, ARRAY_FILTER_USE_KEY);

        $loader = AliasLoader::getInstance();
        $loader->setAliases($aliases);
    }
}