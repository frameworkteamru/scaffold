<?php

namespace Dom1no\Scaffold\Template;

use Blade;

class BladeGenerator
{
    public static function all($params)
    {
        $stubPath = config('scaffold.stubPath');

        //$content['migration'] = self::migration($params['migration'], $stubPath);
        $content['base'] = self::base($params, $stubPath);
        $content['model'] = self::model($params, $stubPath);
        $content['controller'] = self::controller($params, $stubPath);
        $content['route'] = self::route($params, $stubPath);

        $content['view'] = self::view($params, $stubPath);

        return $content;
    }

    public static function migration($params, $stubPath)
    {
        $render = view($stubPath . 'migration', $params);

        return $render;
    }

    public static function base($params, $stubPath)
    {
        $render = view($stubPath . 'base', $params);

        return $render;
    }

    public static function model($params, $stubPath)
    {
        $render = view($stubPath . 'model', $params);

        return $render;
    }

    public static function controller($params, $stubPath)
    {
        $render = view($stubPath . 'controller', $params);

        return $render;
    }

    public static function route($params, $stubPath)
    {
        $render = view($stubPath . 'route', $params);

        return $render;
    }

    public static function extraroute($params, $stubPath)
    {
        $render = view($stubPath . 'extraroute', $params);

        return $render;
    }


    public static function config($params,$stubPath)
    {
        $render = view($stubPath . 'config', $params);

        return $render;
    }

    public static function view($params, $stubPath)
    {
        $compiler = app('doggy.compiler');

        $compiler->setContentTags('{%', '%}');
        $compiler->setRawTags('{-%', '%-}');


        $render['index'] = view($stubPath . 'html_assets.index', $params)->render();

        $render['edit'] = view($stubPath . 'html_assets.edit', $params)->render();

        $render['create'] = view($stubPath . 'html_assets.create', $params)->render();

        $render['show'] = view($stubPath . 'html_assets.show', $params)->render();

        $render['item'] = view($stubPath . 'html_assets.item', $params)->render();

        $render['filter'] = view($stubPath . 'html_assets.filter', $params)->render();

        $compiler->setContentTags('{{', '}}');
        $compiler->setRawTags('{!!', '!!}');

        return $render;
    }
}