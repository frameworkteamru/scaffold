<?php
namespace Dom1no\Scaffold\File;

use Dom1no\Scaffold\Helpers\PathHelper;

class MakeFile
{
    public static function all($fileName, $prefix, $views)
    {
        //$paths['migration'] = self::migration($fileName);
        $paths['base'] = self::base($fileName);
        $paths['model'] = self::model($fileName);
        $paths['controller'] = self::controller($fileName, $prefix);
        $paths['route'] = self::route($fileName, $prefix);
        $paths['view'] = self::view($fileName, mb_strtolower($prefix), $views);

        return $paths;
    }

    public static function migration($fileName, $prefix)
    {
        $path = self::makePath('migration', $fileName, $prefix);

        return $path;
    }

    public static function base($fileName)
    {
        $path = self::makePath('base', $fileName);

        return $path;
    }

    public static function model($fileName)
    {
        $path = self::makePath('model', $fileName);

        return $path;
    }

    public static function controller($fileName, $prefix)
    {
        $fileName = $fileName . 'Controller';

        $path = self::makePath('controller', $fileName, $prefix);

        return $path;
    }

    public static function route($fileName, $prefix)
    {
        $path = self::makePath('route', $fileName, $prefix);

        return $path;
    }

    public static function view($fileName, $prefix, $views)
    {
        $paths = self::makePath('view', $fileName, $prefix, $views);

        return $paths;
    }

    protected static function makePath($entity, $fileName, $prefix = null, $views = null)
    {
        $name = $fileName;
        $path = config('scaffold.' . $entity . 's_path');

        if ($entity == 'view')
        {
            return self::getViewPath($fileName, $prefix, $views);
        }

        $path .= ucfirst($prefix) . DIRECTORY_SEPARATOR;

        $path = $path . ucfirst(PathHelper::getRootEntity($name)) . "/" . $name . '.php';

        $path = self::replaceSeparator($path);

        return $path;
    }

    protected static function getViewPath($entity, $prefix, $views)
    {
        if (starts_with($prefix, 'api')) {
                return [];
        }

        $path = config('scaffold.views_path');
        $path .= preg_replace('/\\./', DIRECTORY_SEPARATOR, PathHelper::getViewPath($entity, $prefix));
        
        $paths = [];

        foreach ($views as $view) {
            $paths[$view] = $path . '\\' . $view . '.blade.php';
            $paths[$view] = self::replaceSeparator($paths[$view]);
        }

        return $paths;
    }

    protected static function replaceSeparator($string)
    {
        $string = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $string);

        if (php_sapi_name() == 'cli') {
            $string = str_replace('..', '.', $string);
        }

        return $string;
    }

}