    // Префиксы
    'prefix' => [
        'api' => [
            'Admin',
            'Face'
        ],
        'web' => [
            'Admin',
            'Face'
        ]
    ],

@if (isset($masters))
    // Основные сущности
    'masters' => [
@foreach($masters as $master)
        '{{$master}}',
@endforeach
    ],
@endif

@if (isset($subordinates))
    // Подчинённые сущности
    'subordinates' => [
@foreach($subordinates as $key=>$subordinate)

        '{{$key}}' => [
@foreach($subordinate as $s)
            '{{$s}}',
@endforeach
        ],
@endforeach
    ],
@endif

@if (isset($options))

    'options' => [
        '--force' => true,
@foreach($options as $key => $value)
'{{$key}}' => @if ($value == 1) {{$value}}, @else '{{$value}}', @endif
@endforeach
    ],
@endif
