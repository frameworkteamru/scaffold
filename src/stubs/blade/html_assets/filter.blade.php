<form method="GET">
    <div class="form-group row">
        <label class="col-md-1" for="id">ID</label>
        <div class="col-md-2">
            <input id="id" type="number" name="filter['id']" class="form-control">
        </div>
        <button type="submit" class="btn btn-primary">Search</button>
    </div>
</form>