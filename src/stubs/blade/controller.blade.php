namespace {{ $namespaces['controller'] }};

use App\Http\Requests;
use App\Http\Controllers\Controller;

use {{ $namespaces['model_path'] }};
use Illuminate\Http\Request;

@if(count($parent))
use {{ $parent['namespace'] }};
@endif


class {{ $controllerClass }} extends Controller
{

@if(count($parent))
    public function index({{ $parent['model'] }} ${{ $parent['name'] }})
    {
        ${{ $parent['inversedBy'] }}s = ${{ $parent['name'] }}->{{ $parent['inversedBy'] }}()->orderBy('id', 'desc')->paginate(10);

        return view('{{ $viewPath }}.index', [
            '{{ $parent['inversedBy'] }}s' => ${{ $parent['inversedBy'] }}s,
            '{{ $parent['name'] }}' => ${{ $parent['name'] }}
        ]);
    }

    public function create({{ $parent['model'] }} ${{ $parent['name'] }})
    {
        return view('{{ $viewPath }}.create', [
            '{{ $parent['name'] }}' => ${{ $parent['name'] }}
        ]);
    }

    public function save(Request $request, {{ $parent['model'] }} ${{ $parent['name'] }})
    {
        ${{ $var }} = new {{ $class }}($request->all());

        ${{ $var }}->save();

        return redirect()->route('{{ $viewPrefix }}.index', ${{ $parent['name'] }});
    }

    public function edit({{ $parent['model'] }} ${{ $parent['name'] }}, {{ $class }} ${{ $var }})
    {
        return view('{{ $viewPath }}.edit', [
            '{{ $var }}' => ${{ $var }},
            '{{ $parent['name'] }}' => ${{ $parent['name'] }}
        ]);
    }

    public function update(Request $request, {{ $parent['model'] }} ${{ $parent['name'] }}, {{ $class }} ${{ $var }})
    {
        ${{ $var }}->update($request->all());

        return redirect()->route('{{ $viewPrefix }}.index', ${{ $parent['name'] }});
    }

    public function delete({{ $parent['model'] }} ${{ $parent['name'] }}, {{ $class }} ${{ $var }})
    {
        ${{ $var }}->delete();

        return redirect()->back();
    }
@else
    public function index()
    {
        ${{ $var }}s = {{ $class }}::orderBy('id', 'desc')->paginate(10);

        return view('{{ $viewPath }}.index', [
            '{{ $var }}s' => ${{ $var }}s
        ]);
    }

    public function create()
    {
        return view('{{ $viewPath }}.create');
    }

    public function save(Request $request)
    {
        ${{ $var }} = new {{ $class }}($request->all());

        ${{ $var }}->save();

        return redirect()->route('{{ $viewPrefix }}.index');
    }

    public function edit({{ $class }} ${{ $var }})
    {
        return view('{{ $viewPath }}.edit', [
            '{{ $var }}' => ${{ $var }}
        ]);
    }

    public function update(Request $request, {{ $class }} ${{ $var }})
    {
        ${{ $var }}->update($request->all());

        return redirect()->route('{{ $viewPrefix }}.index');
    }

    public function delete({{ $class }} ${{ $var }})
    {
        ${{ $var }}->delete();

        return redirect()->back();
    }
@endif
}