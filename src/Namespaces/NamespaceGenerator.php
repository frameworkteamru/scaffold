<?php
namespace Dom1no\Scaffold\Namespaces;

use Dom1no\Scaffold\Params\GeneratorParams;
use Dom1no\Scaffold\Helpers\PathHelper;

class NamespaceGenerator
{
	const NAMESPACE_SEPARATOR = '\\';

	public static function generateNamespace($entity, $prefix = null, $path = '', $withPrefix = true)
    {
        $namespace = ucfirst(preg_replace('/^\\.\\.\\//', '',  $path));

        if ($prefix && $withPrefix) $namespace .= $prefix . "/";

        $namespace .= ucfirst(PathHelper::getRootEntity($entity)) . "/";

        return $namespace;
    }

    public static function generateBaseNamespace($entity, $prefix = null)
    {
    	return self::generateNamespace($entity, $prefix, config('scaffold.bases_path'), false);
    }

    public static function generateModelNamespace($entity, $prefix = null)
    {
    	return self::generateNamespace($entity, $prefix, config('scaffold.models_path'), false);
    }

    public static function generateControllerNamespace($entity, $prefix = null)
    {
    	return self::generateNamespace($entity, $prefix, config('scaffold.controllers_path'));
    }

    public static function generateRouteNamespace($entity, $prefix = null)
    {
    	return rtrim(self::generateNamespace($entity, $prefix, '', false), '/');
    }

    public static function generateNamespaces($entity, $prefix = null)
    {
        $namespaces = [];
        $namespaces['base'] = self::generateBaseNamespace($entity, $prefix);
        $namespaces['model'] = self::generateModelNamespace($entity, $prefix);
        $namespaces['controller'] = self::generateControllerNamespace($entity, $prefix);
        $namespaces['route'] = self::generateRouteNamespace($entity, $prefix);
        $namespaces['base_model_path'] = self::generateBaseNamespace($entity, $prefix) . "$entity";
        $namespaces['model_path'] = self::generateModelNamespace($entity, $prefix) . "$entity";

        foreach ($namespaces as $key => $namespace) {
            $namespaces[$key] = ucfirst(self::replaceSeparator(rtrim($namespace, '/')));
        }

        return $namespaces;
    }

    public static function replaceSeparator($string)
    {
        return str_replace(['/', '\\'], self::NAMESPACE_SEPARATOR, $string);
    }
}