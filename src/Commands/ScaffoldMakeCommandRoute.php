<?php

namespace Dom1no\Scaffold\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Dom1no\Scaffold\Template\BladeGenerator;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use File;

class ScaffoldMakeCommandRoute extends Command
{
    protected $name = 'scaffold:route';

    protected $description = 'Make extra route by config/scaffold_project.php';

    protected $meta;

    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;
    }

    public function fire()
    {
        $masters = config('scaffold_project.masters');
        $subordinates = config('scaffold_project.subordinates');
        $includes = config('scaffold.includes');
        $stubPath = config('scaffold.stubPath');

        $property = array();
        $property['route_path'] = config('scaffold.routes_path');
        $prefixes = config('scaffold_project.prefix');;

        foreach ($prefixes as $key => $prefix) {
            foreach ($prefix as $value) {
                $property['route_prefix'] = $value;
                $file_path = 'routes/extra' . ucfirst($key) . $value . 'Routes.php';
                $property['include_path'] = '';

                if (!count($masters)) {
                    abort(500, 'Masters does not exist');
                }

                foreach ($masters as $master) {

                    $this->info('Start ' . $master);
                    $property = self::makePropertyMaster($master, $property, $key);

                    if (array_key_exists($master, $subordinates) && !empty($subordinates[$master])) {
                        foreach ($subordinates[$master] as $subordinate) {
                            $property = self::makePropertySlave($master, $subordinate, $property, $key);
                        }
                    }

                    if (array_key_exists($master, $includes) && !empty($includes[$master])) {
                        foreach ($includes[$master] as $include) {
                            $property = self::makePropertySlave($master, $include, $property, $key);
                        }
                    }

                    $this->info('Finished ' . $master);
                }

                $content = BladeGenerator::extraroute($property, $stubPath);

                try {

                    self::routePut($file_path, $content);
                    $this->info('Route ' . $file_path . ' successfully generated!');

                } catch (Exception $e) {

                    $this->error('Cannot save extraroute: ' . $e->getMessage());

                }
            }
        }
    }

    protected function makePropertyMaster($master, $property, $key)
    {
        $col = preg_match('/[A-Z][^A-Z]*$/', $master, $matches);

        if ($col > 0) {
            $prefix = strtolower($matches[0]);
        } else {
            $prefix = strtolower($master);
        }

        $property['groups'][$master]['prefix'] = $prefix;
        $property['groups'][$master]['as'] = $prefix . '.';
        $property['groups'][$master]['namespace'] = ucfirst($key) . '\\' . $property['route_prefix'] . '\\' . $master;
        $property['groups'][$master]['controller'] = $master . 'Controller';

        return $property;
    }

    protected function makePropertySlave($master, $subordinate, $property, $key)
    {
        $prefix = ((isset($property['route_prefix'])) ? $property['route_prefix'] . '/' : '');
        $path = (($property['include_path']) ? $property['include_path'] . '/' . $prefix : $key . DIRECTORY_SEPARATOR . $prefix);

        $col = preg_match('/[A-Z][^A-Z]*/', $subordinate, $matches);

        if (($col > 0) && ($master != $matches[0]))
        {
            $master_dir = $matches[0];
        }
        elseif (($col > 0) && ($master == $matches[0]))
        {
            $master_dir = $master;
        }
        else
        {
            return $property;
        }

        $property['groups'][$master]['subordinates'][$subordinate]['src'] = $path . $master_dir . '/' . $subordinate . '.php';

        return $property;
    }

    protected function routePut($path, $content)
    {
        $content = '<?php' . PHP_EOL . PHP_EOL . $content;
        File::put($path, $content);
    }

    public function getMeta()
    {
        return $this->meta;
    }

    protected function getArguments()
    {
        return [
            ['name', InputOption::VALUE_REQUIRED, 'The name of the model. (Ex: Post)', null],
        ];
    }
}
