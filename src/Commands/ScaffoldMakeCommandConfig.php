<?php

namespace Dom1no\Scaffold\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Dom1no\Scaffold\Template\BladeGenerator;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

use File;

class ScaffoldMakeCommandConfig extends Command
{
    protected $name = 'scaffold:config';

    protected $description = 'Create config/scaffold_project.php by Doctrine entity yml files';

    protected $meta;


    public function __construct(Filesystem $files)
    {
        parent::__construct();

        $this->files = $files;

    }

    public function fire()
    {
        $path = config('scaffold.metadata_path'); //Doctrine yml
        $stubPath = config('scaffold.stubPath');  //Views path

        $prefix = null;

        $entities['options'] = array();

        if ($this->option('prefix')) {
            $entities['options']['-p'] = $this->option('prefix');
        }

        if ($this->option('model')) {
            $entities['options']['-m'] = true;
        } elseif ($this->option('controller')) {
            $entities['options']['-c'] = true;
        } elseif ($this->option('route')) {
            $entities['options']['-r'] = true;
        } elseif ($this->option('view')) {
            $entities['options']['-v'] = true;
        }

        $files = $this->files->files(base_path($path));

        if (is_array($files) && !empty($files))
        {
            $filenames = self::getAllFilename($files);

            $data = self::generateEntity($filenames);

            $entities = array_merge($data, $entities);

            $content = BladeGenerator::config($entities, $stubPath);

            try {

                $configPath = base_path('config/scaffold_project.php');
                self::configPut($configPath, $content);

            } catch (Exception $e) {

                $this->error('Cannot generate config: ' . $e->getMessage());

            }

            $this->info('Config "' . $configPath . '" successfully generated!');

        } else {

            $this->error('Check "' . $path . '" the presence of the essential files , and try again!');

        }
    }

    /*
     * Recievs 'path/FirstSecond.ext'
     * Returns 'path/without/extension' => ['First', 'Second']
     */
    protected function getAllFilename($files)
    {
        $data = array();

        foreach ($files as $key => $value)
        {
            $name = self::separatePath($value);

            if (!isset($data[$name]))
            {
                preg_match_all('/[A-Z][^A-Z]*/', $name, $broken_name);

                array_shift($broken_name[0]);
                $data[$name] = $broken_name[0];
            }
        }

        return $data;
    }

    /*
     * Returns [ 'masters' => [], 'subordinates => [ 'Master' => 'MasterSubordinate' ] ]
     */
    protected function generateEntity($filenames)
    {
        $masters = array();
        $subordinates = array();

        foreach ($filenames as $filename)
        {
            if (count($filename) == 1) {
                $masters[] = array_shift($filename);
            } else {
                $key = array_shift($filename);
                $subordinates[$key][] = $key . implode('', $filename);
            }
        }

        $result['masters'] = $masters;
        $result['subordinates'] = $subordinates;

        return $result;
    }

    /*
     * returns path without extension replacing all slashes and backslashes on DIRECTORY_SEPARATOR
     */
    protected function separatePath($path)
    {
        $path = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $path);
        $path = explode('\\', $path);
        $path = array_pop($path);
        $path = explode('.', $path);
        $name = array_shift($path);

        return $name;
    }

    protected function getOptions()
    {
        return [
            ['prefix', 'p', InputOption::VALUE_REQUIRED, 'put in desired directory', null],
            ['model', 'm', 1, 'Only make models', null],
            ['controller', 'c', 1, 'Only make controller', null],
            ['route', 'r', 1, 'Only make route', null],
            ['view', 'view', 1, 'Only make views', null],
        ];
    }

    protected function configPut($path, $content)
    {
        $content = '<?php' . PHP_EOL . PHP_EOL . 'return [' . PHP_EOL . PHP_EOL . $content . PHP_EOL . '];';
        File::put($path, $content);
    }

}
