<?php

namespace Dom1no\Scaffold\Commands;

use Illuminate\Console\Command;
use Illuminate\Filesystem\Filesystem;
use Illuminate\Support\Composer;
use Dom1no\Scaffold\Params\GeneratorParams;
use Dom1no\Scaffold\Template\BladeGenerator;
use Dom1no\Scaffold\Makes\MakerTrait;
use Dom1no\Scaffold\File\MakeFile;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use File;
use Exception;

class ScaffoldMakeCommand extends Command
{
    use MakerTrait;

    protected $signature = 'scaffold:make
                            {name? : The name of the model. (Ex: Post)}
                            {--b|base-model : Make base models} 
                            {--m|model : Only make models}
                            {--c|controller : Only make controllers} 
                            {--r|route : Only make route}
                            {--view|view : Only make views}
                            {--f|force : Overwrite any existing files}
                            {--p|prefix= : Put in desired directory}';

    protected $description = 'Create a scaffold with bootstrap 3';

    protected $meta;

    private $composer;

    public function __construct(Filesystem $files, Composer $composer)
    {
        parent::__construct();

        $this->files = $files;
        $this->composer = $composer;
    }

    public function handle()
    {
        GeneratorParams::cleanAliases();

        $entity = $this->argument('name');

        $prefix = null;
        if ($this->option('prefix')) {
            $prefix = ucfirst($this->option('prefix'));
        }

        if (!$entity)
        {
            $this->generateAll();
            die;
        }

        $this->info('Configuring ' . $entity . '...');

        //Генерация параметров
        $params = GeneratorParams::all($entity, $prefix);

        //Генерация контента
        $content = BladeGenerator::all($params);

        //Генерация путей
        $paths = $this->makePaths($entity, $prefix);

        //Создание файлов
        $this->compileFile($paths, $content);

        $this->info('Success!');
    }

    protected function generateAll()
    {
        $entities = GeneratorParams::allEntities();

        foreach($this->option() as $option => $value)
        {
            if ($value)
            {
                $params['--'.$option] = $value;
            }
        }

        if (!isset($params))
        {
            throw new Exception('No parameters are specified with no arguments');
        }

        foreach ($entities as $entity)
        {
            $this->call('make:scaffold', array_merge(['name' => $entity], $params));
        }
    }

    protected function makePaths($entity, $prefix)
    {
        $paths = [];

        if ($this->option('base-model'))
        {
            $paths['base'] = MakeFile::base($entity);
        }
        elseif ($this->option('model'))
        {
            $paths['base'] = MakeFile::base($entity);
            $paths['model'] = MakeFile::model($entity);
        }
        elseif ($this->option('controller'))
        {
            $paths['controller'] = MakeFile::controller($entity, $prefix);
        }
        elseif ($this->option('route'))
        {
            $paths['route'] = MakeFile::route($entity, $prefix);
        }
        elseif ($this->option('view'))
        {
            $paths['view'] = MakeFile::view($entity, $prefix, config('scaffold.views'));
        }
        else
        {
            $paths = MakeFile::all($entity, $prefix, config('scaffold.views'));
        }

        return $paths;
    }

    protected function compileFile($paths, $content)
    {
        foreach ($paths as $key => $path) {

            if ($key == 'view')
            {
                foreach ($path as $name => $value) {

                    $this->makeDirectory($value);

                    $this->filePut($value, $content[$key][$name]);
                }
            }
            else
            {
                $this->makeDirectory($path);

                $content[$key] = '<?php' . PHP_EOL . $content[$key];

                $this->filePut($path, $content[$key]);
            }
        }
    }

    protected function filePut($path, $content)
    {
        if ($this->option('force') || !file_exists($path))
        {
            echo $path . PHP_EOL;
            File::put($path, $content);
        }
    }

    protected function makeDirectory($path)
    {
        if (!File::isDirectory(dirname($path)))
        {
            File::makeDirectory(dirname($path), 0755, true);
        }
    }

    public function getMeta()
    {
        return $this->meta;
    }
}
