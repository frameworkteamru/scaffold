<?php

namespace Dom1no\Scaffold\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class ScaffoldMakeCommandProject extends Command
{
    protected $name = 'scaffold:project';

    protected $description = 'Create a scaffold by config/scaffold_project.php';

    protected $meta;

    public function __construct()
    {
        parent::__construct();
    }

    public function fire()
    {
        $settings = config('scaffold_project');

        if (is_null($settings)) {
            $this->error('Config not found. Please, enter command scaffold:config');
            return;
        }

        $masters = $settings['masters'];
        $subordinates = $settings['subordinates'];

        $options = [];
        $options['--force'] = $this->option('force');

        if (!empty($settings['options'])) {
            $options = $settings['options'];
        }

        $prefixes = self::getPrefixes($settings['prefix']);

        foreach ($masters as $master)
        {
            $arguments = [
                ['-b' => true],
                ['-m' => true],
                ];
            foreach ($arguments as $argument)
            {
                $params = array_merge($options, $argument);
                self::generateScaffold($master, $params);

                if (array_key_exists($master, $subordinates) && !empty($subordinates[$master]))
                {
                    foreach ($subordinates[$master] as $subordinate)
                    {
                        self::generateScaffold($subordinate, $params);
                    }
                }
            }

            $arguments = [
                ['-c' => true],
                ['-r' => true],
                ['--view' => true],
                ];
            foreach ($prefixes as $prefix)
            {
                $options['--prefix'] = $prefix;
                foreach ($arguments as $argument)
                {
                    $params = array_merge($options, $argument);
                    self::generateScaffold($master,  $params);

                    if (array_key_exists($master, $subordinates) && !empty($subordinates[$master]))
                    {
                        foreach ($subordinates[$master] as $subordinate)
                        {
                            self::generateScaffold($subordinate, $params);
                        }
                    }
                }
            }
        }
    }

    protected function generateScaffold($name, array $arguments)
    {
        $arguments['name'] = ucfirst($name);

        $this->call('scaffold:make', $arguments);
    }
    /*
     * Returns array of prefix paths from config array prefix tree
     */
    private function getPrefixes($configPrefix)
    {
        $prefixes = [];

        foreach ($configPrefix as $key => $prefix) {
            foreach ($prefix as $value) {
                $prefixes[] = $key . DIRECTORY_SEPARATOR . $value;
            }
        }

        return $prefixes;
    }

    protected function getOptions()
    {
        return [
            ['force', 'f', InputOption::VALUE_NONE, 'Overwrite any existing files', null],
        ];
    }

    public function getMeta()
    {
        return $this->meta;
    }
}
