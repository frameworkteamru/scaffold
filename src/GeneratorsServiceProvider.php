<?php

namespace Dom1no\Scaffold;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Tools\Setup;

use Dom1no\Scaffold\Template\DoggyBladeCompiler;
use Illuminate\View\Engines\CompilerEngine;

class GeneratorsServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $routeConfig = [
            'namespace' => 'Dom1no\Scaffold\Controllers',
            'prefix' => '_scaffold',
        ];

        $this->app['router']->group($routeConfig, function($router) {
            $router->get('build:{entity?}', [
                'uses' => 'MakeController@build',
                'as' => 'scaffold.build',
            ]);
        });

        $this->loadViewsFrom(__DIR__.'/stubs/blade', 'blade');

        $this->publishes([
            __DIR__.'/stubs/blade' => base_path('resources/views/vendor/blade'),
        ]);

        $this->publishes([__DIR__ . '/config/scaffold.php' => config_path('scaffold.php')]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/config/scaffold.php', 'scaffold');

        $this->registerScaffoldGenerator();
        $this->registerScaffoldGeneratorConfig();
        $this->registerScaffoldGeneratorProject();
        $this->registerScaffoldGeneratorRoute();
        $this->registerEntityManager();
        $this->registerDoggyEngine();
    }

    /**
     * Register the make:scaffold generator.
     */
    private function registerScaffoldGenerator()
    {
        $this->app->singleton('command.larascaf.scaffold', function ($app) {
            return $app['Dom1no\Scaffold\Commands\ScaffoldMakeCommand'];
        });

        $this->commands('command.larascaf.scaffold');
    }
    /**
     * Register the scaffold:config generator.
     */
    private function registerScaffoldGeneratorConfig()
    {
        $this->app->singleton('command.larascaf.scaffold.config', function ($app) {
            return $app['Dom1no\Scaffold\Commands\ScaffoldMakeCommandConfig'];
        });

        $this->commands('command.larascaf.scaffold.config');
    }
    /**
     * Register the scaffold:project generator.
     */
    private function registerScaffoldGeneratorProject()
    {
        $this->app->singleton('command.larascaf.scaffold.project', function ($app) {
            return $app['Dom1no\Scaffold\Commands\ScaffoldMakeCommandProject'];
        });

        $this->commands('command.larascaf.scaffold.project');
    }
    /**
     * Register the scaffold:project generator.
     */
    private function registerScaffoldGeneratorRoute()
    {
        $this->app->singleton('command.larascaf.scaffold.route', function ($app) {
            return $app['Dom1no\Scaffold\Commands\ScaffoldMakeCommandRoute'];
        });

        $this->commands('command.larascaf.scaffold.route');
    }
    /**
     * Register the make:scaffold generator.
     */
    private function registerEntityManager()
    {
        $this->app->singleton('em', function ($app) {

            $dbParams = array(
                'driver'   => 'pdo_mysql',
                'user'     => env('DB_USERNAME', 'root'),
                'password' => env('DB_PASSWORD', ''),
                'dbname'   => env('DB_DATABASE', '')
            );

            $classLoader = new \Doctrine\Common\ClassLoader(null, base_path(config('scaffold.metadata_entity_path')));
            $classLoader->register();
            $config = Setup::createYAMLMetadataConfiguration(array(base_path(config('scaffold.metadata_path'))), true);

            $entityManager = EntityManager::create($dbParams, $config);

            return $entityManager;
        });
    }

    public function registerDoggyEngine()
    {
        $this->app->singleton('doggy.compiler', function () {
            return new DoggyBladeCompiler(
                $this->app['files'], $this->app['config']['view.compiled']
            );
        });

        $this->app['view']->addExtension('doggy.php', 'blade', function () {
            return new CompilerEngine($this->app['doggy.compiler']);
        });
    }
}
