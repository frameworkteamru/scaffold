<?php
namespace Dom1no\Scaffold\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Dom1no\Scaffold\Params\GeneratorParams;
use Dom1no\Scaffold\Template\BladeGenerator;
use Dom1no\Scaffold\File\MakeFile;
use DB;

class MakeController extends Controller
{
    public function build($entity = 'Group')
    {
        GeneratorParams::cleanAliases();

        $stubPath = config('scaffold.stubPath');
        $views = config('scaffold.views');
        $params = GeneratorParams::all($entity, null);

        $content = BladeGenerator::all($params, $stubPath);
        $paths = MakeFile::all($entity, null ,$views);

        return view('blade::http/build', ['paths' => $paths, 'render' => $content['base']->render()]);
    }
}
