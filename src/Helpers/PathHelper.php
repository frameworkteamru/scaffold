<?php
namespace Dom1no\Scaffold\Helpers;

use Dom1no\Scaffold\Namespaces\NamespaceGenerator;

class PathHelper
{
	public static function getRootEntity($entity)
    {
        return preg_replace('/[A-Z].*/', '', lcfirst($entity));
    }

    public static function getChildOrRootEntity($entity)
    {
        return preg_replace('/^.*([A-Z][a-z]+)$/', '\1', $entity);
    }

    public static function getViewPath($entity, $prefix)
    {
        $prefix = self::stripWebOrApi($prefix);
        $subPath = (strtolower(self::getChildOrRootEntity($entity)) == strtolower($entity)) ? "" : self::getChildOrRootEntity($entity);
        $viewPath = strtolower(strtolower(str_replace(['/', '\\'], '.', NamespaceGenerator::generateNamespace($entity, $prefix) . $subPath)));

        return rtrim($viewPath, ".");
    }

    public static function getViewPrefix($entity, $prefix)
    {
        $viewPrefix =  strtolower(self::getChildOrRootEntity(lcfirst($entity)));
        $root = self::getRootEntity($entity);

        if ($viewPrefix != $root) {
            $viewPrefix = $root . '.' . $viewPrefix;
        }

        if ($prefix) {
            $viewPrefix = strtolower($prefix) . '.' . $viewPrefix;
            $viewPrefix = self::stripWebOrApi($viewPrefix);
        }

        return $viewPrefix;
    }

    public static function stripWebOrApi($string)
    {
    	return preg_replace("/^(api|web)\\//i", '', $string);
    }
}