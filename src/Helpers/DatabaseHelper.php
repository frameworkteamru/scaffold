<?php

namespace Dom1no\Scaffold\Helpers;

use Dom1no\Scaffold\Namespaces\NamespaceGenerator;
use Dom1no\Scaffold\Helpers\PathHelper;

use DB;

class DatabaseHelper
{
    const ONE_TO_ONE = 1;
    const MANY_TO_ONE = 2;
    const ONE_TO_MANY = 4;
    const MANY_TO_MANY = 8;
    const NAMESPACE_SEPARATOR = '\\';

    private static $relationship = [];
    private static $relationshipName = null;

    public static function getTableName($entity)
    {
        $table = strtolower(preg_replace('/[A-Z]/', '_$0', lcfirst($entity)));

        return preg_replace('/_/', '__', $table);
    }

    public static function getManager()
    {
        return DB::connection('mysql')->getDoctrineSchemaManager();
    }

    public static function getTableColumns($table)
    {
        $manager = self::getManager();

        $listFields = $manager->listTableColumns($table);

        return $listFields;
    }

    public static function getTablePrimary($table)
    {
        $manager = self::getManager();

        return $manager->listTableDetails($table)->getPrimaryKey()->getColumns()[0];
    }

    public static function getFillable($table)
    {
        $listFields = self::getTableColumns($table);
        $primary = self::getTablePrimary($table);

        unset($listFields['deleted_at'], $listFields['created_at'], $listFields[$primary]);

        return $listFields;
    }

    public static function getOther($table)
    {
        $vars = [];
        $listFields = self::getTableColumns($table);

        if (isset($listFields['deleted_at'])) {
            $vars['softDeletes'] = true;
        }

        if (isset($listFields['created_at']) && isset($listFields['updated_at'])) {
            $vars['timestamp'] = true;
        }
        else
        {
            $vars['timestamp'] = false;
        }

        return $vars;
    }

    public static function getFillableAndOther($table)
    {
        $listFields = self::getFillable($table);
        $vars = self::getOther($table);

        $data['fillable'] = array_keys($listFields);
        $data['vars'] = $vars;

        return $data;
    }

    public static function getRelationship($entity)
    {
        $relationships = self::getAssociationMappings($entity);

        $vars = [];

        if (count($relationships))
        {
            foreach ($relationships as $relationship)
            {
                self::$relationshipName = array_get($relationship, 'fieldName');
                self::$relationship = $relationship;

                if ($relationship['isCascadeRemove']) $vars['cascadeSoftDeletes'][] = strtolower(self::$relationshipName);

                $vars['relationships'][self::$relationshipName] = self::getRelationshipData();
            }
        }

        return $vars;
    }

    public static function getParentRelationship($entity)
    {
        $relationships = self::getRelationship($entity);
        $parentEntity = PathHelper::getRootEntity($entity);

        $parentRelationship = [];

        if (isset($relationships['relationships']) && count($relationships['relationships']))
        {
            foreach ($relationships['relationships'] as $name => $relationship)
            {
                if ($name == $parentEntity && $relationship['type'] == 'belongsTo')
                {
                    $parentRelationship['name'] = $relationship['name'];
                    $parentRelationship['namespace'] = $relationship['path'];

                    $parts = explode(self::NAMESPACE_SEPARATOR, $relationship['path']);

                    $parentRelationship['model'] = array_pop($parts);
                    $parentRelationship['inversedBy'] = $relationship['inversedBy'];
                }
            }
        }

        return $parentRelationship;
    }

    public static function getChildRelationships($entity)
    {
        $relationships = self::getRelationship($entity);
        $childRelationships = [];

        if (isset($relationships['relationships']) && count($relationships['relationships']))
        {
            foreach ($relationships['relationships'] as $name => $relationship)
            {
                $targetEntity = ucfirst(PathHelper::getRootEntity($relationship['targetEntity']));
                if ($targetEntity == $entity && ($relationship['type'] == 'hasMany' || $relationship['type'] == 'hasOne')) {
                    $childRelationships[] = $relationship['targetEntity'];
                }
            }
        }

        return $childRelationships;
    }

    public static function getAssociationMappings($entity)
    {
        $em = app('em');
        $relationships = $em->getMetadataFactory();
        $relationships = $relationships->getMetadataFor($entity);
        $relationships = $relationships->associationMappings;

        return $relationships;
    }

    private static function getRelationshipData()
    {
        $data = [];

        $type = array_get(self::$relationship, 'type');
        $targetEntity = array_get(self::$relationship, 'targetEntity');
        $inversedBy = array_get(self::$relationship, 'inversedBy');
        $params = self::getRelationParams($targetEntity, $type);

        $data['type'] = self::getRelationType($type);
        $data['name'] = strtolower(self::$relationshipName);
        $data['path'] = NamespaceGenerator::generateNamespaces($targetEntity)['model_path'];
        $data['local'] = $params['local'];
        $data['foreign'] = $params['foreign'];
        $data['inversedBy'] = $inversedBy;
        $data['relation_table'] = isset($params['joinTable']) ? $params['joinTable'] : null;
        $data['targetEntity'] = $targetEntity;

        return $data;
    }

    public static function getRelationType($type)
    {
        switch ($type) {
            case self::ONE_TO_ONE:
                return isset(self::$relationship['mappedBy']) ? "hasOne" : "belongsTo";
            case self::ONE_TO_MANY:
                return "hasMany";
            case self::MANY_TO_ONE:
                return "belongsTo";
            case self::MANY_TO_MANY:
                return "belongsToMany";
            default:
                break;
        }
    }

    public static function getRelationParams($targetEntity, $type)
    {
        if ($type == self::MANY_TO_MANY)
        {
            $params = self::getParamsForM2M($targetEntity);
        }
        else
        {
            $params = self::getParamsForOther($targetEntity);
        }

        return $params;
    }

    private static function getParamsForM2M($targetEntity)
    {
        $params = [];
        $joinTable = self::getJoinTable($targetEntity);

        $params['foreign'] = array_get($joinTable, 'inverseJoinColumns.0.name');
        $params['local'] = array_get($joinTable, 'joinColumns.0.name');
        $params['joinTable'] = array_get($joinTable, 'name');

        return $params;
    }

    private static function getJoinTable($targetEntity)
    {
        if (self::$relationship['isOwningSide'])
        {
            $joinTable = self::$relationship['joinTable'];
        }
        else
        {
            $name = lcfirst(self::$relationship['mappedBy']);

            $metadata = app('em')->getMetadataFactory()->getMetadataFor($targetEntity);

            if (isset($metadata->associationMappings[$name]))
            {
                $joinTable = $metadata->associationMappings[$name]['joinTable'];
            }
            else {
                throw new \Exception("Invalid foreign table name " . $name, 1);
            }
        }

        return $joinTable;
    }

    private static function getParamsForOther($targetEntity)
    {
        $params = [];

        if (self::$relationship['isOwningSide'])
        {
            $joinColumn = array_get(self::$relationship, 'joinColumns.0');
            $params['local'] = array_get($joinColumn, 'name');
            $params['foreign'] = array_get($joinColumn, 'referencedColumnName');
        } else {
            $name = strtolower(self::$relationship['mappedBy']);
            $joinColumn = array_get(
                app('em')->getMetadataFactory()
                ->getMetadataFor($targetEntity)
                ->associationMappings,
                "{$name}.joinColumns.0"
            );

            $params['local'] = array_get($joinColumn, 'referencedColumnName');
            $params['foreign'] = array_get($joinColumn, 'name');
        }

        return $params;
    }
}