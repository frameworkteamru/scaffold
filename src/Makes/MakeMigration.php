<?php
/**
 * Created by PhpStorm.
 * User: fernandobritofl
 * Date: 4/22/15
 * Time: 10:34 PM
 */

namespace Dom1no\Scaffold\Makes;


use Illuminate\Filesystem\Filesystem;
use Dom1no\Scaffold\Commands\ScaffoldMakeCommand;
use Dom1no\Scaffold\Migrations\SchemaParser;
use Dom1no\Scaffold\Migrations\SyntaxBuilder;

class MakeMigration {
    use MakerTrait;

    protected $scaffoldCommandObj;

    public function __construct(ScaffoldMakeCommand $scaffoldCommand, Filesystem $files)
    {
        $this->files = $files;
        $this->scaffoldCommandObj = $scaffoldCommand;

        $this->start();
    }


    protected function start(){

        $name = $this->scaffoldCommandObj->getObjName('Name');

        $params = $this->help($name);

        $vars['class'] = $params['name'];

        $content = view('dom1no.scaffold.src.stubs.migration', $vars);

        $name = 'create_'.str_plural(strtolower( $this->scaffoldCommandObj->argument('name') )).'_table';

        // Verifica se o arquivo existe com o mesmo o nome
        if ($this->files->exists($path = $this->getPath($name)))
        {
            return $this->scaffoldCommandObj->error($this->type.' already exists!');
        }

        // Cria a pasta caso nao exista
        $this->makeDirectory($path);

        // Grava o arquivo
        $this->files->put($path, $content);

        $this->scaffoldCommandObj->info('Migration created successfully');
    }


    /**
     * Get the path to where we should store the migration.
     *
     * @param  string $name
     * @return string
     */
    protected function getPath($name)
    {
        return './database/migrations/'.date('Y_m_d_His').'_'.$name.'.php';
    }



    /**
     * Compile the migration stub.
     *
     * @return string
     */
    protected function compileMigrationStub()
    {
        $stub = $this->files->get(config('scaffold.stubPath') . 'migration.stub');

        $this->replaceClassName($stub)
            ->replaceSchema($stub)
            ->replaceTableName($stub);


        return $stub;
    }





    /**
     * Replace the class name in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceClassName(&$stub)
    {
        $className = ucwords(camel_case('Create'.str_plural($this->scaffoldCommandObj->argument('name')).'Table'));
        $stub = str_replace('{{class}}', $className, $stub);

        return $this;
    }

    /**
     * Replace the table name in the stub.
     *
     * @param  string $stub
     * @return $this
     */
    protected function replaceTableName(&$stub)
    {
        $table = $this->scaffoldCommandObj->getMeta()['table'];
        $stub = str_replace('{{table}}', $table, $stub);

        return $this;
    }

    /**
     * Replace the schema for the stub.
     *
     * @param  string $stub
     * @param string $type
     * @return $this
     */
    protected function replaceSchema(&$stub, $type='migration')
    {
        if ($schema = $this->scaffoldCommandObj->option('schema')) {
            $schema = (new SchemaParser)->parse($schema);
        }


        if($type == 'migration'){
            // Create migration fields
            $schema = (new SyntaxBuilder)->create($schema, $this->scaffoldCommandObj->getMeta());
            $stub = str_replace(['{{schema_up}}', '{{schema_down}}'], $schema, $stub);


        } else if($type='controller'){
            // Create controllers fields
            $schema = (new SyntaxBuilder)->create($schema, $this->scaffoldCommandObj->getMeta(), 'controller');
            $stub = str_replace('{{model_fields}}', $schema, $stub);


        } else {}

        return $this;
    }

}