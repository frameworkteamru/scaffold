<?php
/**
 * Created by PhpStorm.
 * User: fernandobritofl
 * Date: 4/21/15
 * Time: 4:58 PM
 */

namespace Dom1no\Scaffold\Makes;


use Illuminate\Filesystem\Filesystem;
use Dom1no\Scaffold\Commands\ScaffoldMakeCommand;
use Dom1no\Scaffold\Migrations\SchemaParser;
use Dom1no\Scaffold\Migrations\SyntaxBuilder;
use Blade;

class MakeView
{
    use MakerTrait;


    protected $scaffoldCommandObj;
    protected $viewName;


    public function __construct(ScaffoldMakeCommand $scaffoldCommand, Filesystem $files, $viewName)
    {
        $this->files = $files;
        $this->scaffoldCommandObj = $scaffoldCommand;
        $this->viewName = $viewName;

        $this->start();
    }

    private function start()
    {
        $this->generateView($this->viewName); // index, show, edit and create
    }

    protected function generateView($nameView = 'index')
    {
        $name = $this->scaffoldCommandObj->getObjName('name');

        $params = $this->help($name);
        $name = $params['name'];

        $var = lcfirst($name);

        $vars['class'] = $name;
        $vars['var'] = $var;
        $vars['doggy'] = "@";

        Blade::setContentTags('[%', '%]');
        Blade::setRawTags('[-%', '%-]');

        $content = view('dom1no.scaffold.src.stubs.html_assets.' . $nameView, $vars);

        // Get path
        $path = $this->getPath($name, 'view-' . $nameView);

        // Create directory
        $this->makeDirectory($path);

        if ($this->files->exists($path)) {
            if ($this->scaffoldCommandObj->confirm($path . ' already exists! Do you wish to overwrite? [yes|no]')) {
                // Put file
                $this->files->put($path, $content);
            }
        } else {
            // Put file
            $this->files->put($path, $content);
        }
    }
}