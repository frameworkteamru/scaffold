<?php

namespace Dom1no\Scaffold\Makes;

use Illuminate\Filesystem\Filesystem;
use Dom1no\Scaffold\Commands\ScaffoldMakeCommand;

class MakeModel {

    use MakerTrait;

    public function __construct(ScaffoldMakeCommand $scaffoldCommand, Filesystem $files)
    {
        $this->files = $files;
        $this->scaffoldCommandObj = $scaffoldCommand;

        $this->start();
    }


    private function start()
    {
        $name = $this->scaffoldCommandObj->getObjName('Name');
        $namespace = substr(config('scaffold.models_path'), 2, -1);
        $baseModelPath = substr(config('scaffold.base_models_path'), 2, -1);

        $params = $this->help($name, $namespace, $baseModelPath);
        $name = $params['name'];

        $namespace = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $params['namespace']);
        $baseModel = $params['model'] . '\\' . $name;
        $baseModel = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $baseModel);

        $vars['namespace'] = $namespace;
        $vars['base_model'] = $baseModel;
        $vars['class'] = $name;

        $content = view('dom1no.scaffold.src.stubs.model', $vars);

        $path = $this->getPath($name, 'model');

        if ($this->files->exists($path)) {
            return $this->scaffoldCommandObj->error($name . ' already exists!');
        }

        $this->makeDirectory($path);

        $this->files->put($path, $content);
        $this->scaffoldCommandObj->info('Model created successfully.');

    }
}
