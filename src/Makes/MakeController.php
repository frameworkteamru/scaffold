<?php
namespace Dom1no\Scaffold\Makes;

use Illuminate\Console\AppNamespaceDetectorTrait;
use Illuminate\Filesystem\Filesystem;
use Dom1no\Scaffold\Commands\ScaffoldMakeCommand;
use Dom1no\Scaffold\Migrations\SchemaParser;
use Dom1no\Scaffold\Migrations\SyntaxBuilder;


class MakeController
{
    use AppNamespaceDetectorTrait, MakerTrait;

    protected $scaffoldCommandObj;

    function __construct(ScaffoldMakeCommand $scaffoldCommand, Filesystem $files)
    {
        $this->files = $files;
        $this->scaffoldCommandObj = $scaffoldCommand;

        $this->start();
    }

    private function start()
    {
        $name = $this->scaffoldCommandObj->getObjName('Name');
        $namespace = substr(config('scaffold.controllers_path'), 2, -1);
        $modelPath = substr(config('scaffold.models_path'), 2, -1);

        $params = $this->help($name, $namespace, $modelPath);
        $name = $params['name'];

        $namespace = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $params['namespace']);

        $modelPath = $params['model'] . '\\' . $name;
        $modelPath = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $modelPath);

        $class = $name . 'Controller';
        $var = lcfirst($name);

        $vars['namespace'] = $namespace;
        $vars['model_path'] = $modelPath;
        $vars['class'] = $class;
        $vars['var'] = $var;
        $vars['model_name_class'] = $name;

        $content = view('dom1no.scaffold.src.stubs.controller', $vars);

        if ($this->files->exists($path = $this->getPath($class))) {
            return $this->scaffoldCommandObj->error($name . ' already exists!');
        }

        $this->makeDirectory($path);

        $this->files->put($path, $content);

        $this->scaffoldCommandObj->info('Controller created successfully.');

        //$this->composer->dumpAutoloads();
    }
}