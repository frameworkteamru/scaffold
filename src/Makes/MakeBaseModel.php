<?php

namespace Dom1no\Scaffold\Makes;

use Illuminate\Filesystem\Filesystem;
use Dom1no\Scaffold\Commands\ScaffoldMakeCommand;

class MakeBaseModel {

    use MakerTrait;

    public function __construct(ScaffoldMakeCommand $scaffoldCommand, Filesystem $files)
    {

        $this->files = $files;
        $this->scaffoldCommandObj = $scaffoldCommand;

        $this->start();
    }


    private function start()
    {
        $name = $this->scaffoldCommandObj->getObjName('Name');
        $namespace = substr(config('scaffold.base_models_path'), 2, -1);

        $params = $this->help($name, $namespace);

        $namespace = str_replace(['/', '\\'], DIRECTORY_SEPARATOR, $params['namespace']);

        $vars['namespace'] = $namespace;
        $vars['class'] = $params['name'];

        $content = view('dom1no.scaffold.src.stubs.basemodel', $vars);

        $path = $this->getPath($name, 'base');

        if ($this->files->exists($path)) {
            return $this->scaffoldCommandObj->error($name . ' already exists!');
        }

        $this->makeDirectory($path);

        $this->files->put($path, $content);

        $this->scaffoldCommandObj->info('BaseModel created successfully.');
    }
}
